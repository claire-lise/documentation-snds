# Documentation collaborative du SNDS
<!-- SPDX-License-Identifier: MPL-2.0 -->

Bienvenue sur la documentation collaborative du Système National des Données de Santé.

Cette documentation est en construction, via [ce dépôt GitLab](https://gitlab.com/healthdatahub/documentation-snds).

## Contributeurs 

Cette documentation sous licence ouverte est maintenue par la DREES et l'INDS.

Elle résulte d'une mise en commun de documents et travaux par plusieurs organisations :
- la Caisse nationale d'assurance maladie - [Cnam](https://www.ameli.fr/)
- l'Institut National des Données de Santé - [INDS](https://www.indsante.fr/)
- la Direction de la Recherche, des études, de l’évaluation et des statistiques - 
[DREES](https://drees.solidarites-sante.gouv.fr/etudes-et-statistiques/la-drees/)


## Organisation la documentation

La documentation est organisée en 5 sections :
- [introduction](introduction/README.md) est un guide introductif au SNDS ;
- [fiche thématiques](fiches/README.md) contient des fiches thématiques détaillés ;
- [ressources](ressources/README.md) liste des liens vers différentes ressources documentaires sur le SNDS ;
- La section **Tables** présente l'ensemble des produits, tables et variables du SNDS ;
- [contribuer](contribuer/README.md) est un guide de contribution à cette documentation.

Chacune de ces sections correspond à un dossier sur [GitLab](https://gitlab.com/healthdatahub/documentation-snds), avec deux dossiers supplémentaires pour les [fichiers](https://gitlab.com/healthdatahub/documentation-snds/files) et [images](https://gitlab.com/healthdatahub/documentation-snds/images).


::: tip Note
La section **Tables** est générée automatiquement à partir du [schema formalisé du SNDS](https://gitlab.com/healthdatahub/schema-snds), qui alimente également le [dictionnaire interactif](http://dico-snds.health-data-hub.fr/).
:::

## Licence

Ce dépôt est publié par la DREES et l'Institut National des Données de Santé sous
licence Mozilla Public License 2.0.

Voir le fichier [LICENSE](https://gitlab.com/healthdatahub/documentation-snds/blob/master/LICENSE).
